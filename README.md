Requirements:
 - node >= 12

Command for install packages and dependencies to run locally: 
 - `npm install`

Run tests:
 - `npx wdio run ./apps.conf.ts --suite example --Denv=HI5QA`

Run static analizye of code:
 - `npx eslint **/*.ts`