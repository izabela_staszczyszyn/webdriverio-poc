import { DataHelper } from "./DataHelper";

export class CancerInfoHelper {

    public static getTumorInformationContent(): any {
        return CancerInfoHelper.formTumorInformationContent(
            'Adamantinoma of long bones (C40._)',
            '9261/3',
            'Overlapping lesion of breast',
            'C50.8',
            `Test location description abc${DataHelper.getPhiPiiString()}`,
            'Size',
            `Test other description small${DataHelper.getPhiPiiString()}`,
            '2.16.840.1.113883.6.43.1',
            '20190301')
    }

    public static formTumorInformationContent(type: string, termCodeType: string, location: string, termCodeLocation: string, locationDescription: string, other: string, otherDescription: string, system: string, version: string): any {
        return {
            type,
            termCodeType,
            location,
            termCodeLocation,
            locationDescription,
            other,
            otherDescription,
            system,
            version
        };
    }


}