import moment = require("moment");
import { DateType } from "./enums/DateType";

export class DateHelper {

    static getPastDateForAge(cntOfYears: number, cntOfMonths: number, cntOfDays: number):any {
        return moment().subtract(cntOfYears, 'years').subtract(cntOfMonths, 'months').subtract(cntOfDays, 'days').format("MM/DD/YYYY");
    }

    static formatDateForDateSelector(date:any): string {
        let day = date.getDate();
        day = day < 10 ? '0' + day : day;
        let month = date.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        const year = date.getFullYear();
        return '' + month + day + year;
    }

    static getUniqueTimestamp(): string {
        return new Date().getTime().toString(36) + Math.floor(Math.random() * 1000);
    }

    static getFormattedDate(date: any, initialFormat: string, finalFormat: string): any {
        const dateMoment = initialFormat ? moment(date, initialFormat) : moment(date);
        return dateMoment.format(finalFormat);
    }

    static getPastDate(type: DateType, offset = 0): any {
        const pastDate = new Date();
        switch (type) {
            case DateType.MONTH:
                pastDate.setMonth(pastDate.getMonth() - 1 - offset);
                break;
            case DateType.DAY:
                pastDate.setDate(pastDate.getDate() - 1 - offset);
                break;
            case DateType.HOUR:
                pastDate.setHours(pastDate.getHours() - 1 - offset);
                break;
            case DateType.YEAR:
                pastDate.setFullYear(pastDate.getFullYear() - 1 - offset);
                break;
        }
        return pastDate;
    }

}