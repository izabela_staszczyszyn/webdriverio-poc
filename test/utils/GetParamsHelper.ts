export function getParam(name:string){
    const param = process.argv.filter((element) => element.match(name))[0];

    if (param) {
        return param.split('=')[1];
    }
    return null;
}