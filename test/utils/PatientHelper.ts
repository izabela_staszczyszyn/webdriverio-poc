import { DataHelper } from "./DataHelper";
import { DateHelper } from "./DateHelper";
import { DateType } from "./enums/DateType";

export class PatientHelper {
    public static getNewPatientData(dobType = '', firstName = '', lastName = ''): any {
        let pastDate
        let patientFirstName
        let patientLastName
        if (dobType === 'age') { pastDate = DateHelper.getPastDateForAge(27, 9, 0) } else { pastDate = DateHelper.formatDateForDateSelector(DateHelper.getPastDate(DateType.YEAR)) }

        if (firstName === '') {
            patientFirstName = `000 UIAutomationTest${DataHelper.getPhiPiiString()}-${DateHelper.getUniqueTimestamp()}`;
        } else { patientFirstName = firstName; }

        if (lastName === '') {
            patientLastName = `0000 Dynamic${DataHelper.getPhiPiiString()}`;
        } else { patientLastName = lastName; }

        return PatientHelper.formPatientContent(
            patientFirstName,
            patientLastName,
            pastDate,
            'Male',
            DataHelper.getNewUUID(),
        )
    }

    static formPatientContent(firstName: string, lastName: string, dateOfBirth: string, gender: string, mrn: string): any {
        const patient = {
            fullName: `${lastName}, ${firstName}`,
            firstName,
            lastName,
            dateOfBirth,
            gender,
            mrn
        };

        return patient;
    }
}