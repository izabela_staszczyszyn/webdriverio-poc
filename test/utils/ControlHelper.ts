import utils from "../configs/utils";

class ControlHelper {
    readonly conf = utils.getEnvFile();

    async runTumorBoard(token: Promise<string>) {
        const tokenValue: string = await token.then(value => value);

        browser.setCookies([
            { name: 'access_token', value: tokenValue.slice(7), path: '/', domain: '.platform.navify.com' }
        ]);

        await browser.url(this.conf.baseUrl);
        await browser.pause(10000);
    }

}
export default new ControlHelper();