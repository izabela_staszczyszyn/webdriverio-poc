import utils from "../configs/utils";
import axios from "axios";
import { RestHelper } from "./RestHelper";
import logger from '@wdio/logger'
import { RequestType } from "../utils/enums/RequestType";

export class RestApiClient {
    public finalUser: any;
    private logger: any = logger('[RestApiClient]');
    private host: any;
    private restHelper: RestHelper;
    readonly conf = utils.getEnvFile();

    constructor(restHelper: RestHelper) {
        this.finalUser = restHelper.finalUser;
        this.host = this.conf.baseUrl.replace(this.conf.params.login.tenantAlias, restHelper.finalUser.tenantAlias)
        this.restHelper = restHelper;
    }

    private async getHeaders(): Promise<any> {
        const token: any = await this.restHelper.token;
        return {
            'X-Navify-Tenant': this.restHelper.finalUser.tenantAlias,
            'Authorization': token
        };
    }

    public async get(endpoint: string): Promise<any> {
        return await this.getCall(endpoint, RequestType.GET);
    }

    public async put(endpoint: string, json: any): Promise<any> {
        return await this.getCall(endpoint, RequestType.PUT, json);
    }

    public async post(endpoint: string, json: any): Promise<any> {
        return await this.getCall(endpoint, RequestType.POST, json);
    }

    public async delete(endpoint: string): Promise<any> {
        return await this.getCall(endpoint, RequestType.DELETE);
    }

    private async getCall(endpoint: string, requestType: RequestType, json: any = null): Promise<any> {
        const finalUrl = this.host + endpoint;
        let response:any

        try {
            response = await (axios({
                method: requestType,
                url: finalUrl,
                headers: await this.getHeaders(),
                data: json
            }));
            this.logger.info(`[${requestType.toUpperCase()}] Success call for "${endpoint}"`);

        } catch (err) {
            this.logger.console.error(response);
            this.logger.console.error(err);
        }
        return response;
    }
}