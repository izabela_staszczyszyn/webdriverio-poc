import axios from "axios";
import { Buffer } from "buffer";
import utils from "../configs/utils";

export class RestHelper {
    finalUser: any;
    readonly conf = utils.getEnvFile();
    token: Promise<string>;

    constructor(user = utils.getEnvFile().params.login) {
        this.finalUser = user;
        this.token = this.getToken();
    }

    private async getConf() {
        return (await axios.get(this.conf.baseUrl + 'tumor-board/api/v1/ui-configuration')).data;
    }

    private async getAuth(oktaUrl: string) {
        const authBody = {
            "username": this.finalUser.email,
            "password": this.finalUser.password
        };

        return (await (axios.post(oktaUrl + '/api/v1/authn', authBody))).data;
    }

    private async getAuthorize(platform: any, sessionToken: string) {
        const state = Buffer.from(JSON.stringify({
            "providerType": "okta",
            "returnBy": "postMessage"
        })).toString("base64");

        return (await axios.get(platform.oktaUrl + '/oauth2/default/v1/authorize', {
            params: {
                client_id: platform.oktaClientId,
                response_type: 'code',
                prompt: 'none',
                scope: 'openid offline_access profile',
                redirect_uri: platform.apiGatewayURL + "/login_v2",
                sessionToken: `${sessionToken}`,
                state: `${state}`
            }
        }))
    }

    async getToken(): Promise<string> {
        const conf: any = (await this.getConf());
        const oktaUrl: string = conf.platform.oktaUrl;

        const sessionToken: string = (await this.getAuth(oktaUrl)).sessionToken;
        const authorize: any = (await this.getAuthorize(conf.platform, sessionToken));

        return await ('Bearer ' + authorize.headers['set-cookie'][0].split("=")[1].split(';')[0]);
    }
}
