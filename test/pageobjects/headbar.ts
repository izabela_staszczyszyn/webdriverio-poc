import appPatientSearch from "./appPatientSearch";
import clinicalTrialMatchResults from "./apps/clinical-trial-match-results";
import Page from "./page";

class Headbar extends Page {

    get component() { return $('app-headbar') }
    get searchButton() { return this.component.$('.headbar__search-button') }

    get appsForm() { return $('[data-e2e-label="headbar-nav-apps"]') }
    get apps() { return $$('nap-launchpad-button button div.icon') }

    getAppsTextValue(): string {
        return this.getText(this.appsForm);
    }

    searchPatient(name: string, fullText: any = null) {
        this.click(this.searchButton);
        appPatientSearch.component.waitForDisplayed();
        appPatientSearch.select(name, fullText);
    }

    gotToCTM() {
        this.click(this.appsForm);
        this.click(this.apps[0]);
        this.switchOnCurrentFrame();
        clinicalTrialMatchResults.component.waitForDisplayed();
    }

    gotToGDL() {
        this.click(this.appsForm);
        this.click(this.apps[0]);
        this.switchOnCurrentFrame();
        clinicalTrialMatchResults.component.waitForDisplayed();
    }
}
export default new Headbar();