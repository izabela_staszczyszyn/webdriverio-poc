
export default class Page {

    getText(locator: $): string {
        locator.waitForDisplayed();
        return locator.getText();
    }

    click(locator: $): void {
        locator.waitForDisplayed();
        locator.waitForClickable();
        locator.click();
    }

    getFrames(): any {
        return $$('iframe')
    }

    switchOnCurrentFrame() {
        // browser.waitUntil(() => this.getFrames().length > 1,
        // { timeout: 20000, timeoutMsg: 'New Frame is not present on the page' });
        browser.pause(5000);
        browser.switchToFrame(this.getFrames()[0]);
    }

    waitForElementisInvisible(locator: $): void {
        locator.waitForDisplayed();
        browser.waitUntil(() => !locator.isDisplayed(), { timeout: 15000, timeoutMsg: 'Element is still displayed' });
    }

    // waitForListIsVisible(list: $$): void {
    //     browser.waitUntil(() => list.length > 0, { timeout: 10000, timeoutMsg: 'List is empty' });

    // }
}
