import { Apps } from "../utils/enums/Apps";
import clinicalTrialMatchResults from "./apps/clinical-trial-match-results";
import Page from "./page";

class AppPatientMenu extends Page {
    get component() { return $('app-patient-menu') }

    get appsButtons() { return this.component.$('nap-launchpad-button').$$('button') }


    public goToApp(app: Apps, hasResults = false): void {
        switch (app) {
            case Apps.CTM:
                this.goToCTM(hasResults);
                break;
            case Apps.GDL:
                this.goToGDL(hasResults);
                break;
            case Apps.PS:
                this.goToPS(hasResults)
                break;
            default:
                break;
        }
    }

    private goToPS(hasResults: boolean):void {
        this.click(this.appsButtons[1]);
    }

    private goToGDL(hasResults: boolean):void {
        this.click(this.appsButtons[2]);
    }

    private goToCTM(hasResults: boolean):void {
        this.click(this.appsButtons[0]);
        this.switchOnCurrentFrame();
        clinicalTrialMatchResults.component.waitForDisplayed();

        if (hasResults) {
            expect(clinicalTrialMatchResults.matchList[0]).toBeDisplayed();
        } else {
            expect(clinicalTrialMatchResults.emptyState).toBeDisplayed();
        }
    }
}
export default new AppPatientMenu()