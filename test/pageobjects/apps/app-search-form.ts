import Page from "../page";
import clinicalTrialMatchResults from "./clinical-trial-match-results";

class AppSearchForm extends Page {
    get component() { return $('nap-search-form') }
    get searchField() { return this.component.$('input') }
    get loader() { return $('nap-spinner') }

    get helpMoreButton() { return this.component.$('header__help').$('button') }
    get printButton() { return this.component.$('.print-button') }

    public search(value: string, isResult = true): void {
        this.searchField.setValue(value);
        browser.keys("Enter");

        this.waitForElementisInvisible(this.loader);
        if (isResult) { clinicalTrialMatchResults.matchList[0].waitForDisplayed(); }
    }

    public clickPrintButton() {
        this.click(this.printButton);
        browser.pause(15000);
    }
}
export default new AppSearchForm();