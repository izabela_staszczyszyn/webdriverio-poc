import Page from "../page";

class ClinicalTrialMatchResults extends Page {
    get component(){return $('nap-search-results')}
    get emptyState(){return this.component.$('nap-empty-state')}
    get matchList(){return this.component.$('nap-clinical-trials-list-item').$$('h2')}

}
export default new ClinicalTrialMatchResults();