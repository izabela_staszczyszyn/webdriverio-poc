
class Utils {
    private readParamsFromCli(): any {
        const paramsPair = process.argv.slice(3).filter(it => {
            return it.startsWith('--D');
        });

        const params: any = {};
        paramsPair.forEach(pair => {
            const parts = pair.split('=');
            const name = parts[0].trim().replace('--D', '');
            const value = parts[1] && parts[1].trim() || true;
            params[name] = value;
        });

        return params;
    }

    public getEnvFile(): any {
        const params = this.readParamsFromCli();
        try {
            return require(`./${params.env.toLocaleString().toLowerCase()}.js`);
        } catch (error) {
            throw new Error(`Missing or wrong params --Denv : ${error}`);
        }
    }

}
export default new Utils();