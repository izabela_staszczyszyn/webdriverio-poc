import { PatientCalls } from "../../api/PatientCalls";
import { RestHelper } from "../../api/RestHelper";
import appPatientMenu from "../../pageobjects/appPatientMenu";
import appSearchForm from "../../pageobjects/apps/app-search-form";
import headbar from "../../pageobjects/headbar";
import controlHelper from "../../utils/ControlHelper";
import { Apps } from "../../utils/enums/Apps";
import { PatientHelper } from "../../utils/PatientHelper";

describe('@GDL-2535 Bullets Points in PDF output - patients perspective', () => {
    const restHelper = new RestHelper();
    const patientCalls = new PatientCalls(restHelper);
    const patientData = PatientHelper.getNewPatientData();

    let patientUUID: number;

    beforeAll(async () => {
        patientUUID = await patientCalls.createPatient(patientData);
        await patientCalls.addTumorInformation(patientUUID, false, true, true);
        await controlHelper.runTumorBoard(restHelper.token);
    })

    it('Find patient', () => {
        headbar.searchPatient(patientData.mrn);
    });

    it('CTM app is displayed', () => {
        appPatientMenu.goToApp(Apps.CTM, true);
    });

    it('Click print', () => {
        appSearchForm.clickPrintButton();
    });

    it('print', () => {
        const nodeKeySender = require('node-key-sender');
        nodeKeySender.sendKey('enter');
        browser.pause(5000);
        nodeKeySender.sendText('test');
        nodeKeySender.sendKey('enter');
        nodeKeySender.sendKey('enter');
        browser.pause(5000);
    });

    afterAll(async () => {
        await patientCalls.deletePatient(patientUUID);
    })
});